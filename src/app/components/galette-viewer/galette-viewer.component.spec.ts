import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GaletteViewerComponent } from './galette-viewer.component';

describe('GaletteViewerComponent', () => {
  let component: GaletteViewerComponent;
  let fixture: ComponentFixture<GaletteViewerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GaletteViewerComponent]
    });
    fixture = TestBed.createComponent(GaletteViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
