import { AfterViewInit, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-galette-viewer',
  templateUrl: './galette-viewer.component.html',
  styleUrls: ['./galette-viewer.component.less']
})
export class GaletteViewerComponent implements AfterViewInit, OnChanges, OnDestroy {


  @Input() nbrPersonne: number = 0;

  private readonly initialFarineQty: number = 100;
  private readonly initialEggQty: number = 2;
  private readonly initialWaterQty: number = 200;

  public finalFarineQty: number = 0;
  public finalEggQty: number = 0;
  public finalWaterQty: number = 0;

  public isInitialized: boolean = false;

  ngAfterViewInit(): void {
    this.isInitialized = true;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['nbrPersonne']?.currentValue) {
      this.finalEggQty = changes['nbrPersonne'].currentValue * this.initialEggQty;
    }
  }

  ngOnDestroy(): void {
    console.log("JE SUIS DETRUIT")
  }



}
