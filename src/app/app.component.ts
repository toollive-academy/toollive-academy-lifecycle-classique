import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'toollive-academy-lifecycle';

  public personNbr: number = 0;

  public shouldDisplayViewer: boolean = true;

  public personNbrClick(number: number) {
    this.personNbr = number;
    console.log(number)
  }

  public rollGaletteClick() {
    this.shouldDisplayViewer = !this.shouldDisplayViewer;
  }
}
